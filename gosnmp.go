package gosnmp

import (
	"encoding/asn1"
	"fmt"
	"net"
	"reflect"
	"strconv"
	"strings"
)

const (
	SnmpV1  = 0
	SnmpV2c = 1
)

type Varbind struct {
	OID interface{}
	Val asn1.RawValue
}

type SnmpPdu struct {
	Requestid   int
	Errorstatus int
	Erroridx    int
	Varbindlist []Varbind
}

type GetRequest struct {
	Version  int
	Comunity []byte
	Data     SnmpPdu `asn1:"tag:0"`
}

type GetNextRequest struct {
	Version  int
	Comunity []byte
	Data     SnmpPdu `asn1:"tag:1"`
}

type GetResponse struct {
	Version  int
	Comunity []byte
	Data     SnmpPdu `asn1:"tag:2"`
}

type SetRequest struct {
	Version  int
	Comunity []byte
	Data     SnmpPdu `asn1:"tag:0"`
}

type SNMP struct {
	IPaddr    string
	Version   int
	Community string
}

func oidConvert(oid string) (oidObj asn1.ObjectIdentifier, err error) {
	tmpStr := strings.Split(oid, ".")
	var i int64
	for x := range tmpStr {
		i, err = strconv.ParseInt(tmpStr[x], 10, 32)
		if err != nil {
			return
		}
		oidObj = append(oidObj, int(i))
	}
	return
}

func (snmp *SNMP) setupGets(gr interface{}, x int64) {
	v := reflect.ValueOf(gr).Elem()
	v.FieldByName("Version").SetInt(int64(snmp.Version))
	v.FieldByName("Comunity").SetBytes([]byte(snmp.Community))
	v.FieldByName("Data").FieldByName("Requestid").SetInt(x)
	v.FieldByName("Data").FieldByName("Errorstatus").SetInt(0)
	v.FieldByName("Data").FieldByName("Erroridx").SetInt(0)
	return
}

func (snmp *SNMP) sendRequest(sendData []byte) (returndata []byte) {
	returndata = make([]byte, 4096)
	conn, err := net.Dial("udp", snmp.IPaddr)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	defer conn.Close()
	fmt.Fprintf(conn, string(sendData))
	n, err := conn.Read(returndata)
	if err != nil {
		return
	}
	returndata = returndata[:n]
	return
}

func (snmp *SNMP) Get(oid string, x int64) (Gr2 *GetResponse, reqid int64, err error) {
	oidInt, err := oidConvert(oid)
	if err != nil {
		return
	}
	var tmp GetRequest
	snmp.setupGets(&tmp, x)
	//TODO: Setup a method to allow for multiple OID's to be sent
	vb := Varbind{
		(asn1.ObjectIdentifier(oidInt)),
		(asn1.RawValue{Tag: 5, Class: 0, IsCompound: false, Bytes: []byte{}})}
	tmp.Data.Varbindlist = append(tmp.Data.Varbindlist, vb)
	marshalData, err := asn1.Marshal(tmp)
	if err != nil {
		return
	}
	response := snmp.sendRequest(marshalData)
	Gr2 = new(GetResponse)
	_, err = asn1.Unmarshal(response, Gr2)
	return Gr2, x, nil
}

func (snmp *SNMP) GetNext(oid string, x int64) (Gr2 *GetResponse, reqid int64, err error) {
	oidInt, err := oidConvert(oid)
	if err != nil {
		return
	}
	var tmp GetNextRequest
	snmp.setupGets(&tmp, x)
	vb := Varbind{
		(asn1.ObjectIdentifier(oidInt)),
		(asn1.RawValue{Tag: 5, Class: 0, IsCompound: false, Bytes: []byte{}})}
	tmp.Data.Varbindlist = append(tmp.Data.Varbindlist, vb)
	marshalData, err := asn1.Marshal(tmp)
	if err != nil {
		return
	}
	response := snmp.sendRequest(marshalData)
	Gr2 = new(GetResponse)
	_, err = asn1.Unmarshal(response, Gr2)
	return Gr2, x, nil
}
